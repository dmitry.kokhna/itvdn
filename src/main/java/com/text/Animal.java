package com.text;

public enum Animal {
    DOG("gaf",2), CAT("may",4), FROG("kva",5);
    private String voice;
    private int ageAnimal;

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public int getAgeAnimal() {
        return ageAnimal;
    }

    public void setAgeAnimal(int ageAnimal) {
        this.ageAnimal = ageAnimal;
    }

    Animal(String voice, int ageAnimal) {
        this.voice = voice;
        this.ageAnimal = ageAnimal;
    }
}






