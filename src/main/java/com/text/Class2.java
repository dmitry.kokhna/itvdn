package com.text;

public class Class2 {
    private int id;
    private Animal animal;
    private InClass inclass; // переменная Иннер типа ИнКласс

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Animal getAnimal() {
        return animal;
    }
    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public InClass getInclass() {
        return inclass;
    }
    public void setInclass(InClass inclass) {
        this.inclass = inclass;
    }

    //метод с внутренним классом
    public void metodClass(){
        System.out.println("Это метод с внутренним классом");
        class InMetodClass {
           // int i=5;
            {
                System.out.println("Внури класса");
            }
                  public int MetodInClass(int i){
                   //   System.out.println(6);
                return i++;
               }
        }
 InMetodClass inMetodClass=new InMetodClass();
    }
    // конструктор для Class2
    public Class2(int id) {
        this.id = id;
          }

    // Внутренний клас
    public class InClass{
        private int id2;
        public int getId2() {
            return id2;
        }
        public void setId2(int id2) {
            this.id2 = id2;
        }
          }
    }



